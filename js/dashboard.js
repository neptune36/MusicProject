$(document).ready(function(){
  var zoneAjoutChamp= document.getElementById('fields_genre');
  var nbChamp=1;

   var str_genres = new String("");
     $.getJSON('/MusicProject/services/genre/all', function(data) {
        console.log(data);
        $.each(data, function(key,value) {
         str_genres=str_genres.concat("<option value='"+value.infos.key+"'>"+value.infos.name+"</option>");
        })
     });

  document.getElementById('checkMusic').checked = true;
  document.getElementById('checkAlbum').checked = true;
  document.getElementById('checkArtist').checked = true;


  $("#interval_year").slider({});
  $("#year").slider({});
  $("#interval_duration").slider({});
  $("#duration").slider({});


  $("#interval_year").slider("disable");

   $("#interval_year_radio").click(function() {
    $("#interval_year").slider("enable");
    $("#year").slider("disable");
  });

   $("#year_radio").click(function() {
    $("#interval_year").slider("disable");
    $("#year").slider("enable");
  });

  $("#plus").on('click', function() {


     nbChamp=nbChamp+1;
     var group = document.createElement("div");
     group.id="group_genre["+(nbChamp-1)+"]";
     group.setAttribute("class", "input-group group_champ");

     var moins = document.createElement("button");
     moins.type="button";
     moins.setAttribute("class","btn btn-default");
     moins.innerHTML="-";

     var span = document.createElement("span");
     span.setAttribute("class","input-group-btn");
     span.appendChild(moins);

     var input = document.createElement("select");
     input.name = "genre["+nbChamp+"]";
     input.id  = "genre"+nbChamp;
     input.setAttribute("class", "form-control");
     input.innerHTML=str_genres;

     group.appendChild(span);
     group.appendChild(input);

     zoneAjoutChamp.appendChild(group);

     $(moins).click(function() {
        var id_click = moins.parentNode.parentNode.id;
        zoneAjoutChamp.removeChild(document.getElementById(id_click));
        nbChamp=nbChamp-1;
        
      });


  });

});

function disable_search() {

  if(document.getElementById("keyword").value=="")
  {
    if (!$('#checkYear').is(":checked") && !$('#checkGenre').is(":checked") && !$('#checkMusic').is(":checked") && !$('#checkArtist').is(":checked") && !$('#checkAlbum').is(":checked")) { 
      return false;
    }

    if (!$('#checkYear').is(":checked") && !$('#checkGenre').is(":checked")) { 
      if($('#checkMusic').is(":checked") && $('#checkArtist').is(":checked") && $('#checkAlbum').is(":checked")) {
          return false;
      }
      if($('#checkMusic').is(":checked") || $('#checkArtist').is(":checked") || $('#checkAlbum').is(":checked")) {
          return false;
      }
    }

  }
  
  return true;

}