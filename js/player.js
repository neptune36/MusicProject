var currentFile = "";
var play = false;
var lastBtnPlayed = null;
function musicManager(btnMusic) {
	if (window.HTMLAudioElement) {
                try {
                    var oAudio = document.getElementById('player');
                    var btn = btnMusic; 
                    var audioURL = btn.getAttribute("data-music"); 

                    //Skip loading if current file hasn't changed.
                    if (audioURL !== currentFile) {
                        oAudio.src = audioURL;
                        currentFile = audioURL;
                        if(lastBtnPlayed==null|| play==false){
                        	lastBtnPlayed=btn;
                       	}
                        else if(play==true){
                        	play=false;
                        	$(lastBtnPlayed).children().toggleClass('glyphicon-play').toggleClass('glyphicon-stop');
                        	lastBtnPlayed=btn;
                        }
                                     
                    }

                    // Tests the paused attribute and set state. 
                    if (play==false) {
                        oAudio.play();
                        $(btn).children().toggleClass('glyphicon-play').toggleClass('glyphicon-stop');
                        play=true;
                    }
                    else {
                        oAudio.pause();
                        oAudio.currentTime=0;
                        $(btn).children().toggleClass('glyphicon-stop').toggleClass('glyphicon-play');
                        play=false;
                        
                    }
                }
                catch (e) {
                    // Fail silently but show in F12 developer tools console
                     if(window.console && console.error("Error:" + e));
                }
    }
}

$( "#player" ).on( "ended", function() {
    $(lastBtnPlayed).children().toggleClass('glyphicon-play').toggleClass('glyphicon-stop');
    play=false;
});