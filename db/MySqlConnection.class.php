<?php

class MySqlConnection {

    private $PDOInstance = null;
    private static $instance = null;

    const USER = 'root';
    const HOST = 'localhost';
    const PASS = '';
    const DTB = 'music';

    private function __construct() {
        try {
            $this->PDOInstance = new PDO('mysql:dbname=' . self::DTB . ';host=' . self::HOST, self::USER, self::PASS, array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new MYSQLConnection();
        }
        return self::$instance;
    }

    public function query($query) {
        return $this->PDOInstance->query($query);
    }

    public function prepare($query) {
        return $this->PDOInstance->prepare($query);
    }

    public function exec($query) {
        try {
            $v = $this->PDOInstance->exec($query);
        } catch (PDOException $e) {
            var_dump($e);
            return $e->getMessage();
        }var_dump($v);
        return true;
    }

}
