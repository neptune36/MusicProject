<?php

class AlbumManager extends AbstractManager {

    protected $filters = array('title', 'cover', 'release_date');
    protected $index = 'title';

    public function get($key) {

        $splited = split('_', $key);
        $nodes = array('artists', $splited[0], 'albums', $key);
        $album = $this->db->getBranch($nodes);
        if (is_null($album)) {
            return null;
        }
        $artist = $this->db->getBranch(array('artists', $splited[0], 'infos'));
        $album['artist']['infos'] = $artist;
        foreach ($album['genres'] as $genre_key) {
            $genre = $this->db->getBranch(array('genres', $genre_key));
            $genres[$genre_key] = $genre;
        }
        $album['genres'] = $genres;

        $min = 0;
        $sec = 0;

        foreach ($album['tracks'] as $track) {
            $min += $track['infos']['duration']['min'];
            $sec += $track['infos']['duration']['sec'];
        }

        $min += (int) ($sec / 60);
        $sec = $sec % 60;

        $album['infos']['duration'] = array('min' => $min, 'sec' => $sec);

        return $album;
    }

    public function del($key) {
        $splited = split('_', $key);
        $nodes = array('artists', $splited[0], 'albums', $key);
        $this->db->delBranch($nodes);
    }

    public function add($data, $key = null) {
        if (is_null($key)) {
            return null;
        }
        $filtered = $this->filter($data);

        if ($filtered != null) {
            $splited = split('_', $key);
            $nodes = array('artists', $splited[0], 'albums');
            $key .= '_' . Utils::Slug($filtered[$this->index]);
            array_push($nodes, $key);
            $filtered['key'] = $key;
            $branch['infos'] = $filtered;
            $this->db->addBranch($branch, $nodes);

            return $key;
        }
        return null;
    }

    public function set($data, $key) {
        if (is_null($key)) {
            return null;
        }
        $filtered = $this->filter($data);

        if ($filtered != null) {
            $splited = split('_', $key);
            $nodes = array('artists', $splited[0], 'albums');
            array_push($nodes, $key);
            $filtered['key'] = $key;
            $branch['infos'] = $filtered;
            $this->db->addBranch($branch, $nodes);


            return $key;
        }
        return null;
    }

    public function all() {
        $albums = array();
        $artists = $this->db->getBranch(array('artists'));
        foreach ($artists as $artist) {
            foreach ($artist['albums'] as $album) {
                $key = $album['infos']['key'];
                $albums[$key] = $this->get($key);
            }
        }
        return $albums;
    }

    public function lastTen() {

        $temps = [];
        $artists = $this->db->getBranch(array('artists'));

        foreach ($artists as $artist_key => $artist_value) {
            foreach ($artist_value['albums'] as $album_key => $album_value) {
                $temp = array($album_key, $album_value['infos']['release_date']);
                array_push($temps, $temp);
            }
        }

        for ($i = 0; $i < count($temps); $i++) {
            for ($j = $i + 1; $j < count($temps); $j++) {
                if ($temps[$j][1] > $temps[$i][1]) {
                    $temp = $temps[$i];
                    $temps[$i] = $temps[$j];
                    $temps[$j] = $temp;
                }
            }
        }
        $end = count($temps) > 10 ? 10 : count($temps);
        $albums = [];
        $album_manager = ManagerFactory::getManager('album');


        for ($i = 0; $i < $end; $i++) {
            array_push($albums, $album_manager->get($temps[$i][0]));
        }
        return $albums;
    }

    public function getYearBounds() {
        $min = 0;
        $max = 0;
        $artists = $this->db->getBranch(array('artists'));
        $first = true;
        foreach ($artists as $artist) {
            foreach ($artist['albums'] as $album) {
                $year = substr($album['infos']['release_date'], 6, 10);
                if ($first) {
                    $first = !$first;
                    $min = $year;
                    $max = $year;
                }
                if ($year > $max) {
                    $max = $year;
                }
                if ($year < $min) {
                    $min = $year;
                }
            }
        }
        return array('min' => $min, 'max' => $max);
    }

}
