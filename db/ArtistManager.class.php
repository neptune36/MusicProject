<?php

class ArtistManager extends AbstractManager {

    protected $filters = array('name', 'picture');
    protected $index = 'name';

    public function get($key) {
        $nodes = array('artists', $key);
        $artist = $this->db->getBranch($nodes);
        foreach ($artist['albums'] as $album_key => $album) {
            $min = 0;
            $sec = 0;
            foreach ($album['tracks'] as $track) {
                $min += $track['infos']['duration']['min'];
                $sec += $track['infos']['duration']['sec'];
            }

            $min += (int) ($sec / 60);
            $sec = $sec % 60;

            $artist['albums'][$album_key]['infos']['duration'] = array('min' => $min, 'sec' => $sec);
        }
        
        return $artist;
    }

    public function del($key) {
        $nodes = array('artists', $key);
        $this->db->delBranch($nodes);
    }

    public function add($data, $key = null) {
        $filtered = $this->filter($data);
        if ($filtered != null) {
            $nodes = array('artists');
            $key = Utils::Slug($filtered[$this->index]);
            array_push($nodes, $key);
            $filtered['key'] = $key;
            $branch['infos'] = $filtered;

            $this->db->addBranch($branch, $nodes);
            return $key;
        }
        return null;
    }

    public function set($data, $key) {
        $filtered = $this->filter($data);

        if ($filtered != null) {
            $nodes = array('artists', $key);
            $branch['infos'] = $filtered;

            $this->db->addBranch($branch, $nodes);
            return $key;
        }
        return null;
    }

    public function all() {
        $nodes = array('artists');
        return $this->db->getBranch($nodes);
    }

}
