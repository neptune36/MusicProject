<?php

class JsonDb {

    private static $_instance;

    private $DB_FILE = "/db/db.json";

    public static function Instance() {

        if (is_null(self::$_instance)) {
            self::$_instance = new JsonDb();
        }
        return self::$_instance;
    }

    private function __construct() {
        $this->DB_FILE = project_path.$this->DB_FILE;
    }

    public function getDb() {
        $db = file_get_contents($this->DB_FILE);
        return json_decode($db, true);
    }

    public function saveDb($db) {
        $datas = json_encode($db, JSON_UNESCAPED_UNICODE);
        file_put_contents($this->DB_FILE, $datas, LOCK_EX);
    }

    public function getBranch($nodes) {
        $db = $this->getDb();
        return $this->nodesLoop($db, $nodes);
    }

    public function delBranch($nodes) {

        if (is_null($this->getBranch($nodes))) {
            return null;
        }
        $db = $this->getDb();
        $keys = $this->nodesToKeys($nodes);
        eval('unset($db' . "$keys);");
        $this->saveDb($db);
        return true;
    }

    public function addBranch($branch, $nodes) {
        
        if (is_null($this->getBranch($nodes))) {
            $db = $this->getDb();
            $keys = $this->nodesToKeys($nodes);
            eval('$db' . "$keys = ".'$branch;');
            $this->saveDb($db);           
        }else{
            $this->updBranch($branch, $nodes);
        }
        return true;
    }
    
    public function updBranch($branch, $nodes){
        $db = $this->getDb();
        $keys = $this->nodesToKeys($nodes);
        eval('$db' . "$keys = array_replace_recursive(".'$db'.$keys.',$branch);');
        $this->saveDb($db);
        return true;
    }
    
    private function nodesToKeys($nodes) {
        $keys = null;
        foreach ($nodes as $node) {
            $keys .= "['$node']";
        }
        return $keys;
    }

    private function nodesLoop($ans, $nodes) {
        if (empty($nodes)) {
            return $ans;
        }
        if (array_key_exists($nodes[0], $ans)) {
            $ans = $ans[$nodes[0]];
            array_shift($nodes);
            return $this->nodesLoop($ans, $nodes);
        }
        return null;
    }

}
