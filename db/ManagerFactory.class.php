<?php

require_once 'db/AbstractManager.class.php';

class ManagerFactory {
    
    public static function getManager($type){
        
        $manager = null;
        
        switch ($type){
            case 'artist' :
                $manager = new ArtistManager();
                break;
            case 'album' :
                $manager = new AlbumManager();
                break;
            case 'genre' :
                $manager = new GenreManager();
                break;
            case 'track' :
                $manager = new TrackManager();
        }
        
        return $manager;   
    }
    
}