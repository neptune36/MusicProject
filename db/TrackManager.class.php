<?php

class TrackManager extends AbstractManager {

    protected $filters = array('title', 'duration', 'preview', 'num');
    protected $index = 'title';

    public function get($key) {
        $splited = split('_', $key);
        $nodes = array('artists', $splited[0], 'albums', $splited[0] . '_' . $splited[1], 'tracks', $key);
        $track = $this->db->getBranch($nodes);
        if (is_null($track))
            return null;

        $album_manager = ManagerFactory::getManager('album');
        $album = $album_manager->get($splited[0] . '_' . $splited[1]);
        $track['album'] = $album;

        return $track;
    }

    public function del($key) {
        $splited = split('_', $key);
        $nodes = array('artists', $splited[0], 'albums', $splited[0] . '_' . $splited[1], 'tracks', $key);
        $this->db->delBranch($nodes);
    }

    public function add($data, $key = null) {

        if (is_null($key)) {
            return;
        }
        $filtered = $this->filter($data);

        if ($filtered != null) {
            $splited = split('_', $key);
            $nodes = array('artists', $splited[0], 'albums', $key, 'tracks',);
            $key .= '_' . Utils::Slug($filtered[$this->index]);
            array_push($nodes, $key);
            $filtered['key'] = $key;
            $branch['infos'] = $filtered;
            $this->db->addBranch($branch, $nodes);
            return $key;
        }
        return null;
    }

    public function set($data, $key) {

        if (is_null($key)) {
            return;
        }
        $filtered = $this->filter($data);

        if ($filtered != null) {
            $splited = split('_', $key);
            $nodes = array('artists', $splited[0], 'albums', $splited[0] . '_' . $splited[1], 'tracks', $key);
            $branch['infos'] = $filtered;
            $this->db->addBranch($branch, $nodes);
            return $key;
        }
        return null;
    }

    public function all() {
        return "not implemented";
    }

}
