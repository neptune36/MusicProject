<?php

require_once 'utils/Utils.class.php';
require_once 'db/JsonDb.class.php';
require_once 'db/AlbumManager.class.php';
require_once 'db/ArtistManager.class.php';
require_once 'db/GenreManager.class.php';
require_once 'db/TrackManager.class.php';

abstract class AbstractManager {

    protected $db;
    protected $filters;
    protected $index;

    public function __construct() {
        $this->db = JsonDb::Instance();
    }

    public function saveResources($source, $type, $key) {
        $data = file_get_contents($source);
        switch ($type) {
            case 'artist' :
            case 'album' :
            case 'genre' :
                $folder = "img";
                $extension = '.jpg';
                break;
            case 'track':
                $folder = "msc";
                $extension = '.mp3';
                break;
        }
        file_put_contents(project_path . "/$folder/$type/$key$extension", $data);
        return url . "resources/$type/$key";
    }

    public abstract function get($key);

    public abstract function del($key);

    public abstract function add($data, $key = null);

    public abstract function set($data, $key);

    public abstract function all();

    public function search($criterias) {

        $answer = array();
        $track_founds = array();
        $artist_founds = array();
        $album_founds = array();
        $artists = $this->db->getBranch(array('artists'));
        $types = array();
        if ($criterias['checkArtist']) {
            array_push($types, 'artist');
        }
        if ($criterias['checkAlbum']) {
            array_push($types, 'album');
        }
        foreach ($artists as $artist_key => $artist) {
            foreach ($artist['albums'] as $album_key => $album) {
                if ($criterias['checkMusic']) {
                    foreach ($album['tracks'] as $track_key => $track) {
                        if (trim($criterias['keyword']) != '') {
                            if (stristr($track['infos']['title'], $criterias['keyword'])) {
                                if ($criterias['checkGenre']) {
                                    foreach ($criterias['genre'] as $genre_param) {
                                        if (in_array($genre_param, $album['genres'])) {
                                            if ($criterias['checkYear']) {
                                                if ($criterias['choice_year'] == 'value') {
                                                    if (substr($album['infos']['release_date'], 6, 10) == $criterias['year']) {//KW+GENRE+Y
                                                        array_push($track_founds, $track_key);
                                                    }
                                                }
                                                if ($criterias['choice_year'] == 'interval') {//KW+GR+IN
                                                    if (substr($album['infos']['release_date'], 6, 10) >= substr($criterias['interval_year'], 0, 4) && substr($album['infos']['release_date'], 6, 10) <= substr($criterias['interval_year'], 5, 8)) {
                                                        array_push($track_founds, $track_key);
                                                    }
                                                }
                                            } else {
                                                array_push($track_founds, $track_key); //KEYWORD + GENRE
                                            }
                                        }
                                    }
                                } else {
                                    if ($criterias['checkYear']) {
                                        if ($criterias['choice_year'] == 'value') {
                                            if (substr($album['infos']['release_date'], 6, 10) == $criterias['year']) {//KW+Y
                                                array_push($track_founds, $track_key);
                                            }
                                        }
                                        if ($criterias['choice_year'] == 'interval') {//KW+IN
                                            if (substr($album['infos']['release_date'], 6, 10) >= substr($criterias['interval_year'], 0, 4) && substr($album['infos']['release_date'], 6, 10) <= substr($criterias['interval_year'], 5, 8)) {
                                                array_push($track_founds, $track_key);
                                            }
                                        }
                                    } else {
                                        array_push($track_founds, $track_key); //KEYWORD
                                    }
                                }
                            }
                        } else {
                            if ($criterias['checkGenre']) {
                                foreach ($criterias['genre'] as $genre_param) {
                                    if (in_array($genre_param, $album['genres'])) {
                                        if ($criterias['checkYear']) {
                                            if ($criterias['choice_year'] == 'value') {
                                                if (substr($album['infos']['release_date'], 6, 10) == $criterias['year']) {//GENRE+Y
                                                    array_push($track_founds, $track_key);
                                                }
                                            }
                                            if ($criterias['choice_year'] == 'interval') {//GR+IN
                                                if (substr($album['infos']['release_date'], 6, 10) >= substr($criterias['interval_year'], 0, 4) && substr($album['infos']['release_date'], 6, 10) <= substr($criterias['interval_year'], 5, 8)) {
                                                    array_push($track_founds, $track_key);
                                                }
                                            }
                                        } else {
                                            array_push($track_founds, $track_key); //GENRE
                                        }
                                    }
                                }
                            } else {
                                if ($criterias['checkYear']) {
                                    if ($criterias['choice_year'] == 'value') {
                                        if (substr($album['infos']['release_date'], 6, 10) == $criterias['year']) {//Y
                                            array_push($track_founds, $track_key);
                                        }
                                    }
                                    if ($criterias['choice_year'] == 'interval') {//IN
                                        if (substr($album['infos']['release_date'], 6, 10) >= substr($criterias['interval_year'], 0, 4) && substr($album['infos']['release_date'], 6, 10) <= substr($criterias['interval_year'], 5, 8)) {
                                            array_push($track_founds, $track_key);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                foreach ($types as $type) {
                    if ($type == 'artist') {
                        $kw_place = $artist['infos']['name'];
                    }
                    if ($type == 'album') {
                        $kw_place = $album['infos']['title'];
                    }
                    if (trim($criterias['keyword']) != '') {
                        if (stristr($kw_place, $criterias['keyword'])) {
                            if ($criterias['checkGenre']) {
                                foreach ($criterias['genre'] as $genre_param) {
                                    if (in_array($genre_param, $album['genres'])) {
                                        if ($criterias['checkYear']) {
                                            if ($criterias['choice_year'] == 'value') {
                                                if (substr($album['infos']['release_date'], 6, 10) == $criterias['year']) {//KW+GENRE+Y
                                                    if ($type == 'artist') {
                                                        array_push($artist_founds, $artist_key);
                                                    }
                                                    if ($type == 'album') {
                                                        array_push($album_founds, $album_key);
                                                    }
                                                }
                                            }
                                            if ($criterias['choice_year'] == 'interval') {//KW+GR+IN
                                                if (substr($album['infos']['release_date'], 6, 10) >= substr($criterias['interval_year'], 0, 4) && substr($album['infos']['release_date'], 6, 10) <= substr($criterias['interval_year'], 5, 8)) {
                                                    if ($type == 'artist') {
                                                        array_push($artist_founds, $artist_key);
                                                    }
                                                    if ($type == 'album') {
                                                        array_push($album_founds, $album_key);
                                                    }
                                                }
                                            }
                                        } else {
                                            if ($type == 'artist') {
                                                array_push($artist_founds, $artist_key);
                                            }
                                            if ($type == 'album') {
                                                array_push($album_founds, $album_key);
                                            }
                                        }
                                    }
                                }
                            } else {
                                if ($criterias['checkYear']) {
                                    if ($criterias['choice_year'] == 'value') {
                                        if (substr($album['infos']['release_date'], 6, 10) == $criterias['year']) {//KW+Y
                                            if ($type == 'artist') {
                                                array_push($artist_founds, $artist_key);
                                            }
                                            if ($type == 'album') {
                                                array_push($album_founds, $album_key);
                                            }
                                        }
                                    }
                                    if ($criterias['choice_year'] == 'interval') {//KW+IN
                                        if (substr($album['infos']['release_date'], 6, 10) >= substr($criterias['interval_year'], 0, 4) && substr($album['infos']['release_date'], 6, 10) <= substr($criterias['interval_year'], 5, 8)) {
                                            if ($type == 'artist') {
                                                array_push($artist_founds, $artist_key);
                                            }
                                            if ($type == 'album') {
                                                array_push($album_founds, $album_key);
                                            }
                                        }
                                    }
                                } else {
                                    if ($type == 'artist') {
                                        array_push($artist_founds, $artist_key);
                                    }
                                    if ($type == 'album') {
                                        array_push($album_founds, $album_key);
                                    }
                                }
                            }
                        }
                    } else {
                        if ($criterias['checkGenre']) {
                            foreach ($criterias['genre'] as $genre_param) {
                                if (in_array($genre_param, $album['genres'])) {
                                    if ($criterias['checkYear']) {
                                        if ($criterias['choice_year'] == 'value') {
                                            if (substr($album['infos']['release_date'], 6, 10) == $criterias['year']) {//GENRE+Y
                                                if ($type == 'artist') {
                                                    array_push($artist_founds, $artist_key);
                                                }
                                                if ($type == 'album') {
                                                    array_push($album_founds, $album_key);
                                                }
                                            }
                                        }
                                        if ($criterias['choice_year'] == 'interval') {//GR+IN
                                            if (substr($album['infos']['release_date'], 6, 10) >= substr($criterias['interval_year'], 0, 4) && substr($album['infos']['release_date'], 6, 10) <= substr($criterias['interval_year'], 5, 8)) {
                                                if ($type == 'artist') {
                                                    array_push($artist_founds, $artist_key);
                                                }
                                                if ($type == 'album') {
                                                    array_push($album_founds, $album_key);
                                                }
                                            }
                                        }
                                    } else {
                                        if ($type == 'artist') {
                                            array_push($artist_founds, $artist_key);
                                        }
                                        if ($type == 'album') {
                                            array_push($album_founds, $album_key);
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($criterias['checkYear']) {
                                if ($criterias['choice_year'] == 'value') {
                                    if (substr($album['infos']['release_date'], 6, 10) == $criterias['year']) {//Y
                                        if ($type == 'artist') {
                                            array_push($artist_founds, $artist_key);
                                        }
                                        if ($type == 'album') {
                                            array_push($album_founds, $album_key);
                                        }
                                    }
                                }
                                if ($criterias['choice_year'] == 'interval') {//IN
                                    if (substr($album['infos']['release_date'], 6, 10) >= substr($criterias['interval_year'], 0, 4) && substr($album['infos']['release_date'], 6, 10) <= substr($criterias['interval_year'], 5, 8)) {
                                        if ($type == 'artist') {
                                            array_push($artist_founds, $artist_key);
                                        }
                                        if ($type == 'album') {
                                            array_push($album_founds, $album_key);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $tracks = array();
        $albums = array();
        $artists = array();

        $manager = ManagerFactory::getManager('track');
        foreach ($track_founds as $key) {
            $tracks[$key] = $manager->get($key);
        }
        $answer['tracks'] = $tracks;


        $manager = ManagerFactory::getManager('artist');
        foreach ($artist_founds as $key) {
            $artists[$key] = $manager->get($key);
        }
        $answer['artists'] = $artists;


        $manager = ManagerFactory::getManager('album');
        foreach ($album_founds as $key) {
            $albums[$key] = $manager->get($key);
        }
        $answer['albums'] = $albums;


        return $answer;
    }

    protected function filter($data) {
        $filtered = null;
        $checked = false;
        foreach ($data as $key => $value) {
            if ($key == $this->index && trim($value) != '') {
                $checked = true;
                break;
            }
        }
        if (!$checked)
            return null;
        foreach ($data as $key => $value) {
            if (in_array($key, $this->filters)) {
                $filtered[$key] = $value;
            }
        }

        return $filtered;
    }

}
