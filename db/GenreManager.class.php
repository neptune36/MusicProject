<?php

class GenreManager extends AbstractManager {

    protected $filters = array('name', 'picture');
    protected $index = 'name';

    public function get($key) {
        $nodes = array('genres', $key);
        return $this->db->getBranch($nodes);
    }

    public function del($key) {
        $nodes = array('genres', $key);
        $this->db->delBranch($nodes);
        
        $artists = $this->db->getBranch(array('artists'));

        foreach($artists as $artist_key => $artist){
            foreach($artist['albums'] as $album_key => $album){
                foreach($album['genres'] as $genre){
                    if($genre == $key){
                        $this->db->delBranch(array('artists',$artist_key,'albums',$album_key,'genres',$key));
                    }
                }
            }
        }
    }

    public function add($data, $key = null) {

        $filtered = $this->filter($data);

        if ($filtered != null) {

            if (is_null($key)) {
                $nodes = array('genres',);
                $key = Utils::Slug($filtered[$this->index]);
                array_push($nodes, $key);
                $filtered['key'] = $key;
                $branch['infos'] = $filtered;
            } else {
                $splited = split('_', $key);
                $nodes = array('artists', $splited[0], 'albums', $key, 'genres');
                $key = Utils::Slug($filtered[$this->index]);
                $branch[$key] = $key;
            }


            $this->db->addBranch($branch, $nodes);
            return $key;
        }
        return null;
    }

    public function set($data, $key) {
        $filtered = $this->filter($data);

        if ($filtered != null) {

            $nodes = array('genres',);
            array_push($nodes, $key);
            $filtered['key'] = $key;
            $branch['infos'] = $filtered;


            $this->db->addBranch($branch, $nodes);
            return $key;
        }
        return null;
    }

    public function clear($key) {
        $splited = split('_', $key);
        $nodes = array('artists', $splited[0], 'albums', $key, 'genres');
        $this->db->delBranch($nodes);
    }

    public function all() {
        $nodes = array('genres');
        return $this->db->getBranch($nodes);
    }

    public function search($criterias) {
        
    }
    
    public function getAlbumsByGenre($key){
        $album_keys = array();
        $artists = $this->db->getBranch(array('artists'));
        foreach ($artists as $artist){
            foreach($artist['albums'] as $album_key => $album){
                foreach($album['genres'] as $genre){
                    if($genre==$key){
                        array_push($album_keys, $album_key);
                    }
                }
            }
        }
        $albums=array();
        $manager = ManagerFactory::getManager('album');
        foreach ($album_keys as $album_key){
            $albums[$album_key] = $manager->get($album_key);
        }
        if(sizeof($albums)==0){
            return null;
        }
        return $albums;
    }

}
