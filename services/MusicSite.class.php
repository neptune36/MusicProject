<?php

require_once ('db/ManagerFactory.class.php');

class MusicSite {

    private $_file = null;

    private function getRequestParams($request) {
        $params = $request->request;
        $mparams = array();
        foreach ($params as $key => $value) {
            $mparams[$key] = $value;
        }
        $file = $request->files->get('upload');
        if (!is_null($file)) {
            $this->_file = $file;
        }
        return $mparams;
    }

    private function commonParamsForTwig() {
        $genre_manager = ManagerFactory::getManager('genre');
        $album_manager = ManagerFactory::getManager('album');
        $params['genres'] = $genre_manager->all();
        $params['bounds'] = $album_manager->getYearBounds();
        $params['url'] = url;
        $params['admin'] = $_SESSION['connected'];
        return $params;
    }

    public function homePage() {
        $album_manager = ManagerFactory::getManager('album');
        $albums = $album_manager->lastTen();
        $params = $this->commonParamsForTwig();
        $params['albums'] = $albums;
        return array('page' => 'home.html', 'params' => $params);
    }

    public function getPage($type, $key) {
        $manager = ManagerFactory::getManager($type);
        if (is_null($manager)) {
            return null;
        }
        if ($key != 'new' && $key != 'all') {
            $data = $manager->get($key);
            if (is_null($data)) {
                return null;
            }
        }
        if ($key == 'all') {
            $data = $manager->all();
        }
        $params = $this->commonParamsForTwig();
        $params[$type] = $data;
        return array('page' => $type . '.html', 'params' => $params);
    }

    public function search($request){
        $manager = ManagerFactory::getManager('artist');
        $answer=$manager->search($request);
        $params = $this->commonParamsForTwig();
        $params['infos'] = $answer;
        return array('page'=>'result.html','params'=>$params);
    }
    
    public function executeAction($type, $key, $action, $request) {

        $params = $this->getRequestParams($request);

        switch ($type) {
            case 'artist' : $data = $this->updArtist($key, $action, $params);
                break;
            case 'genre' : $data = $this->updGenre($key, $action, $params);
                break;
            case 'album' : $data = $this->updAlbum($key, $action, $params);
                break;
            case 'track' : $data = $this->updTrack($key, $action, $params);
                break;
            default : $data = null;
        }

        $this->_file = null;
        return $data;
    }

    private function updArtist($key, $action, $params) {

        switch ($action) {
            case 'del' :
                $manager = ManagerFactory::getManager('artist');
                $new_key = $manager->del($key);
                return array('url' => '');

            case 'add' :
                if ($key == 'new') {
                    $type = 'artist';
                    $manager = ManagerFactory::getManager($type);
                    $new_key = $manager->add($params);
                    if (!is_null($this->_file)) {
                        $url = $manager->saveResources($this->_file, 'artist', $new_key);
                        $params['picture'] = $url;
                        $manager->set($params, $new_key);
                    }
                } else {
                    $type = 'album';
                    $album_manager = ManagerFactory::getManager($type);
                    $params['release_date'] = date("d/m/Y", strtotime($params['release_date']));
                    $new_key = $album_manager->add($params, $key);
                    if (!is_null($this->_file)) {
                        $url = $album_manager->saveResources($this->_file, 'album', $new_key);
                        $params['cover'] = $url;
                        $album_manager->set($params, $new_key);
                    }
                    $genre_manager = ManagerFactory::getManager('genre');
                    foreach ($params['genres'] as $param) {
                        $genre = $genre_manager->get($param);
                        $genre_manager->add($genre['infos'], $new_key);
                    }
                }
                return array('url' => "$type/$new_key");
            case 'set' :
                $type = 'artist';
                $manager = ManagerFactory::getManager($type);
                if (!is_null($this->_file)) {
                    $url = $manager->saveResources($this->_file, 'artist', $key);
                    $params['picture'] = $url;
                }
                $new_key = $manager->set($params, $key);
                return array('url' => "$type/$new_key");
        }
    }

    private function updGenre($key, $action, $params) {
        $manager = ManagerFactory::getManager('genre');
        switch ($action) {
            case 'del':
                $manager->del($key);
                break;
            case 'add':
                $new_key = $manager->add($params);
                if (!is_null($this->_file)) {
                    $url = $manager->saveResources($this->_file, 'genre', $new_key);
                    $params['picture'] = $url;
                    $manager->set($params, $new_key);
                }
                break;
            case 'set' :
                if (!is_null($this->_file)) {
                    $url = $manager->saveResources($this->_file, 'genre', $key);
                    $params['picture'] = $url;
                }
                $manager->set($params, $key);
                break;
        }
        return array('url' => "genre/all");
    }

    private function updAlbum($key, $action, $params) {
        switch ($action) {
            case 'del' :
                $splited = split('_', $key);
                $manager = ManagerFactory::getManager('album');
                $new_key = $manager->del($key);
                return array('url' => "artist/$splited[0]");

            case 'add':
                $manager = ManagerFactory::getManager('track');
                if (!is_null($params['duration_min']) && !is_null($params['duration_sec'])) {
                    $duration = array('min' => $params['duration_min'], 'sec' => $params['duration_sec']);
                    $params['duration'] = $duration;
                }
                
                $new_key = $manager->add($params, $key);
                if (!is_null($this->_file)) {
                    $url = $manager->saveResources($this->_file, 'track', $new_key);
                    $params['preview'] = $url;
                    $manager->set($params, $new_key);
                }
                return array('url' => "album/$key");

            case 'set' :
                $manager = ManagerFactory::getManager('album');
                if (!is_null($params['duration_min']) && !is_null($params['duration_sec'])) {
                    $duration = array('min' => $params['duration_min'], 'sec' => $params['duration_sec']);
                    $params['duration'] = $duration;
                }
                if (!is_null($this->_file)) {
                    $url = $manager->saveResources($this->_file, 'album', $key);
                    $params['cover'] = $url;
                }
                $params['release_date'] = date("d/m/Y", strtotime($params['release_date']));
                $new_key = $manager->set($params, $key);
                $genre_manager = ManagerFactory::getManager('genre');
                $genre_manager->clear($key);
                foreach ($params['genres'] as $param) {
                    $genre = $genre_manager->get($param);
                    $genre_manager->add($genre['infos'], $key);
                }
                return array('url' => "album/$key");
        }
    }

    private function updTrack($key, $action, $params) {

        switch ($action) {
            case 'del' :
                $splited = split('_', $key);
                $manager = ManagerFactory::getManager('track');
                $new_key = $manager->del($key);
                return array('url' => "album/$splited[0]_$splited[1]");

            case 'set' :
                $splited = split('_', $key);
                $manager = ManagerFactory::getManager('track');
                if (!is_null($params['duration_min']) && !is_null($params['duration_sec'])) {
                    $duration = array('min' => $params['duration_min'], 'sec' => $params['duration_sec']);
                    $params['duration'] = $duration;
                }
                if (!is_null($this->_file)) {
                    $url = $manager->saveResources($this->_file, 'track', $key);
                    $params['preview'] = $url;
                }
                $new_key = $manager->set($params, $key);
                return array('url' => "album/$splited[0]_$splited[1]");
        }
    }

}
