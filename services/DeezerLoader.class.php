<?php

include_once 'db/ManagerFactory.class.php';

class DeezerLoader {

    private static $uri = 'https://api.deezer.com/album/';

    public function loadAlbums($start_id, $number) {
        set_time_limit(0);
        $number = abs($number);

        if (!is_numeric($start_id) || !is_numeric($number)) {
            return "erreur de type";
        }

        for ($id = $start_id; $id < $start_id + $number; $id++) {

            $data = json_decode(file_get_contents(self::$uri . $id), true);
            $tracks = $data['tracks']['data'];

            if (sizeof($tracks) > 0) {
                $this->saveData($data);
            }
        }
    }

    private function saveData($data) {

        $artist = $data['artist'];
        $tracks = $data['tracks']['data'];
        $genres = $data['genres']['data'];

        $data['release_date'] = date("d/m/Y", strtotime($data['release_date']));

        $album_manager = ManagerFactory::getManager('album');
        $artist_manager = ManagerFactory::getManager('artist');
        $track_manager = ManagerFactory::getManager('track');
        $genre_manager = ManagerFactory::getManager('genre');

        $artist_key = $artist_manager->add($artist);

        $album_key = $album_manager->add($data, $artist_key);

        $track_num = 1;
        foreach ($tracks as $track) {
            $secondes = $track['duration'];
            $min = (int) ($secondes / 60);
            $sec = $secondes % 60;
            $duration = array('min' => $min, 'sec' => $sec);
            $track['duration'] = $duration;
            $track['num'] = $track_num;
            $track_manager->add($track, $album_key);
            $track_num++;
        }

        foreach ($genres as $genre) {
            $genre_manager->add($genre);
            $genre_manager->add($genre, $album_key);
        }
    }
}
