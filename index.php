<?php
session_start();
error_reporting(E_ERROR | E_PARSE);

use Symfony\Component\HttpFoundation\Request;
use XMLParser\XMLParser;

require __DIR__ . '/vendor/autoload.php';
require_once 'services/DeezerLoader.class.php';
require_once 'services/MusicSite.class.php';
define(url, 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['CONTEXT_PREFIX'] . '/MusicProject/');
define(project_path, 'C:/wamp/www/MusicProject');

$app = new Silex\Application();
$app['debug'] = true;
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/views',
));

$app->get('/resources/{type}/{key}', function ($type, $key) use($app) {
    switch($type){
        case 'artist':
        case 'album':
        case 'genre':
            $folder = 'img';
            $extension = 'jpg';
            break;
        case 'track':
            $folder = 'msc';
            $extension = 'mp3';
            break;
        default:
            return $app->abort(404,"La source demandée n'existe pas");
    }
    $file = project_path."/$folder/$type/$key.$extension";
    if (!file_exists($file)) {
        $app->abort(404,"La ressource demandée n'existe pas");
    }
    if($type!='track'){
        return $app->sendFile($file, 200, ['Content-Type' => 'image/jpeg']);
    }else{
        return $app->sendFile($file, 200, ['Content-Type' => 'audio/mp3']);
    }
});

$app->get('/services/{type}/{key}', function($type,$key) use ($app){
    $manager = ManagerFactory::getManager($type);
    if(is_null($manager)){
        return null;
    }
    if($key=='all'){
        return json_encode($manager->all());
    }
    return json_encode($manager->get($key));
});

$app->get('/services/{type}/{key}/xml', function($type,$key) use ($app){
    $manager = ManagerFactory::getManager($type);
    if(is_null($manager)){
        return null;
    }
    if($type=='genre'){
        $data = $manager->getAlbumsByGenre($key);
        if(!is_null($data)){
            $xml = XMLParser::encode($data,'albums');
            Utils::Download(sys_get_temp_dir().'-'.$key.'.xml',$xml->asXML());
            return $app->abort(200);
        }
    }
    return $app->abort(404);
});

$app->get('/load/{start_id}:{numbers}', function ($start_id, $numbers) use ($app) {
    
    $deezer = new DeezerLoader();
    $deezer->loadAlbums($start_id, $numbers);
    return "fin";
});

$app->get('/', function () use ($app) {
    
    $music_site = new MusicSite();
    $data = $music_site->homePage();
    return $app['twig']->render($data['page'], $data['params']);
});

$app->get('/search', function (Request $request) use ($app) {
    
    $music_site = new MusicSite();
    $data = $music_site->search($request->query->all());
    return $app['twig']->render($data['page'], $data['params']);
});

$app->get('/{type}/{key}', function ($type,$key) use ($app) {
    
    $music_site = new MusicSite();
    $data = $music_site->getPage($type,$key);
    if(is_null($data))return $app->abort (404);
    return $app['twig']->render($data['page'], $data['params']);
});

//ADMIN
$app->post('/connection', function (Request $request) use ($app){
    $pass = $request->get('pass');
    if ($pass == "root") {
        $_SESSION['connected'] = true;
    }
    
    return $app->redirect($request->server->getHeaders()['REFERER']);
});

$app->get('/disconnection', function () use ($app){
    session_destroy();
    return $app->redirect(url);
});

$app->get('/{type}/{key}/{action}', function ($type,$key,$action) use ($app){
    if(!$_SESSION['connected'])return $app->abort (404);
    $accepted = array('add','set');
    if(!in_array($action, $accepted))return $app->abort (404);
    
    $music_site = new MusicSite();
    $data = $music_site->getPage($type,$key);
    
    if(is_null($data))return $app->abort (404);
    $data['params']['action'] = $action;
    
    if($type=='artist'&&$key!='new'&&$action=='add'){
        $data['params']['type'] = 'album';
    }else{
        $data['params']['type'] = $type;
    }
    if($type=='album'&&$action=='add'){
        $data['params']['type'] = 'track';
    }
    return $app['twig']->render('admin.html', $data['params']);
});

$app->post('/{type}/{key}/{action}', function ($type,$key,$action,Request $request) use ($app){
    if(!$_SESSION['connected'])return $app->abort (404);
    $music_site = new MusicSite();
    $data = $music_site->executeAction($type,$key,$action,$request);
    if(is_null($data))return $app->abort (404);
    return $app->redirect(url.$data['url']);
});

//------------------------------------------------------------------------------
$app->run();